var webpack = require('webpack');

let path = require("path");
const mainsass = path.join("../src/assets/styles");
var HtmlWebpackPlugin = require("html-webpack-plugin");

const ExtractTextPlugin = require('extract-text-webpack-plugin');

const oicomp = path.join(__dirname, "node_modules", "components_oi");
const origin = path.join(__dirname);
const autoprefixer = require('autoprefixer');



module.exports = {
    entry: {
      bundle: [ "./js/index.js"]
    },
    devtool: 'inline-source-map',
    node: {
      fs: 'empty',
    },
    output: {
        path: path.join(__dirname, "./work_folder"),
        filename: "[name].js"
    },
    module:{
      rules: require('./webpack.rules.js')
    },
    plugins: [
      new ExtractTextPlugin('[name].css'),
      new webpack.DefinePlugin({
            'process.env': {
              // This has effect on the react lib size
              'NODE_ENV': JSON.stringify('development'),
            }
      }),
      new webpack.optimize.MinChunkSizePlugin({MinChunkSize: 10000}),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.AggressiveMergingPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.IgnorePlugin(
          /^\.\/locale$/,
          [/moment$/],
          /unicode\/category\/So/,
          /node_modules/
      ),
      new webpack.NamedModulesPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new HtmlWebpackPlugin({
          template: "./index.html",
          'process.env':{
            'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
          }
      }),
    ],
    externals: {
      async: "commonjs async"
    },
    resolve: {
      alias: {
        'react': path.resolve(__dirname, 'node_modules', 'react')
      },
      extensions: ['.jsx','.js', '.json', '.css', '.sass']
    }
};
