import React from "react";
import ReactDOM from "react-dom";

//the main app component
import App from "./components/app.js";

//
import { AppStore } from "./reducers";

//redux config
import { Provider } from 'react-redux';
import { createStore } from 'redux';

let store = createStore(AppStore)

//add router
import { Router, IndexRedirect ,Route, hashHistory } from 'react-router';

//get components to render
const getComponent = require('./components/comp_to_render');

//require comp names
const compNames = require('./components/config').default;


//create routes
let createRoutes = ()=>{
  return compNames
          .filter(function(item){
            return item !== "Container";
          })
          .map((item, key)=>{
              const comp = getComponent(item)
              return <Route
                          key={key}
                          path={`/${item}`}
                          component={comp} />
          })
}



ReactDOM.render(
  <Provider store={store}>
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRedirect to="/Card" />
            {createRoutes()}

        </Route>
    </Router>
  </Provider>
    ,
  document.getElementById('root')
);
