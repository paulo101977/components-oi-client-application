import React, { Component } from "react";

let Popover = require("components_oi")("Popover");

module.exports =
    ()=>{
        return <Popover
                    placement="bottom"
                    content="Overlay Content">
                    Just a popover
                </Popover>
    }
