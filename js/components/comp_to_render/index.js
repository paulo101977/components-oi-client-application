
const names = require('./../config').default;


let getComponents = (name)=>{
    //import any components
    switch(name){
        case "NavMenu":
            return null;
        case "Fiddle":
                return require('./fiddle');
        case "Modal":
            return require("./modal");
        case "Container":
            return require("./content");
        case "Alert":
            return require("./alert");
        case "Collapse":
            return require('./collapse');
        case "ButtonPrimary":
            return require('./button');
        case "Popover":
            return require('./popover');
        case "Input" :
            return require('./input');
        case "TextField":
            return require('./textfield');
        case "Card":
            return require('./card');
        case "Loader":
            return require('./loader');
        case "Breadcrumb":
            return require('./breadcrumb');
        case "Combobox":
            return require('./combobox');
        default:
            return require('./button');

        }
}



module.exports =  getComponents;
