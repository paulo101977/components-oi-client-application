
import React, { Component } from "react";

const Card = require("components_oi")("Card");



module.exports =
    () => {
        return <Card
                    insert={true}
                    title="Just for Test"
                    message="Just a message">
                        <div>Inside children</div>
                </Card>
    };
