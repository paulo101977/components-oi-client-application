import React, { Component } from "react";

let Input = require("components_oi")("Input");



module.exports =
    ()=>{
        return <div>
                    <div className="textfield form-group">
                        <Input
                            type="text"/>
                        <label>Just for test</label>
                    </div>
                    <Input
                        type="checkbox"
                        placeholder="Just for test"/>
                    <div className="textfield form-group">
                        <Input
                            type="password"
                            />
                        <label>Just for test</label>
                    </div>
                </div>
    }
