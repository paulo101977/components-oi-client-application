import React, { Component } from "react";

let Modal = require("components_oi")("Modal");

module.exports =
    ()=>{
        return <Modal
                    id="1"
                    title="Modal"
                    content="Overlay Content">
                    Just a Modal
                </Modal>
    }
