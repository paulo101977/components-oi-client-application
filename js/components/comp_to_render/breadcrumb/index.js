
import React from "react";

let Breadcrumb = require("components_oi")("Breadcrumb");


module.exports = ()=>{
    return <Breadcrumb>
                <Breadcrumb.Item href="#">
                    Home
                </Breadcrumb.Item>
                <Breadcrumb.Item href="http://www.minhaoi.com.br">
                    Minha Oi
                </Breadcrumb.Item>
                <Breadcrumb.Item active>
                    Here
                </Breadcrumb.Item>
            </Breadcrumb>;
}
