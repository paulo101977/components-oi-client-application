import React, { Component } from "react";
import * as Bs from "react-bootstrap";
import user from './img/user.svg';

const compList = require("../components/config").default;

const Mirror = require("./codemirror").default;


//require('../../node_modules/codemirror/lib/codemirror.css');
require('../../scss/codemirror.scss')
require('codemirror/lib/codemirror.css')

//redux bind method
import { connect } from 'react-redux'

const {
        Container,
        Popover,
        NavMenu,
        ButtonPrimary
    } = require("components_oi")(compList);

//set the component selected
let handleClick = (event, obj)=>{

    obj.setState({
        selected: event.currentTarget.textContent
    })
}

let getNavItems = (obj)=>{
    let navitems = [];

    let pathName = obj.props.location.pathname.split('/')[1];

    let byPath = false;

    let selected = obj.state.selected;

    if(obj.isFirst){
      byPath = true;
      obj.isFirst = false;
    }

    compList.forEach(function(item,index){
        if(item !== "Container"){
            navitems.push(
                {
                    title: item,
                    href:"#/" + item,
                    key: index,
                    handleClick: (event) => handleClick(event, obj),
                    active: byPath ? item === pathName : item === selected,
                    item: item
                }
            )
        }
    })

    return navitems;
}


let alertMethod = (event)=>{
    alert('test');
};

//test submenu
let submenu = [
    {
        title: "Home",
        links: [
            {
                handleNavLink: alertMethod,
                linkTitle: 'just a link',
                href:"#/home",
                linkContent: "test"
            }
        ]
    },
    {
        title: "About",
        links: [
            {
                handleNavLink: alertMethod,
                linkTitle: 'just a link',
                href:"#/home",
                linkContent: "test"
            }
        ]
    },
    {
        title: "More",
        links: [
            {
                handleNavLink: alertMethod,
                linkTitle: 'just a link',
                href:"#/home",
                linkContent: "test"
            }
        ]
    }
]

//test menu right content
let rightcontent =
    <Popover
        left='0'
        name='sair'
        className='xs-hidden sair'
        content={<Bs.Button className='link px-2 py-1'>SAIR</Bs.Button>}>
        <span>
            <img className='mr-1' src={user} />
            <span className='sair'>Juscelina Roberta Carvalho Pinto</span>
        </span>
    </Popover>;


class App extends Component {

    constructor(props) {
        super(props);
        /*this.state = {
            selected: 0
        }*/
        this.state = {
          selected: ""
        }

        this.before = "";

        this.isFirst = true;
    }

    componentDidMount() {

      console.log('code app')
      console.log(this.props)

      if(!this.before){
        let path = this.props.location.pathname.split('/')[1];
        this.setState({selected: path})
      }
    }

    componentWillReceiveProps(nextProps){
      const path =
            (nextProps.location && nextProps.location.pathname) ?
                      nextProps.location.pathname.split('/')[1] : ""

      this.before = this.props.location.pathname;
      this.setState({selected: path})
    }

    shouldComponentUpdate(nextProps, nextState) {
      return (this.props.location.pathname
                    !== nextProps.location.pathname)
                        || (this.state.selected
                                  !== nextState.selected);
      //return true;
    }

    render() {
        const isNavMenu = this.props.location.pathname === "/NavMenu";
        let mirrorSize = 7;
        let codeSize = 4;
        let offset = 1;

        if(isNavMenu){
          mirrorSize = codeSize = 12;
          offset = 0;
        }

        return (
            <div>
                <NavMenu
                    rightcontent={rightcontent}
                    submenu={submenu}
                    navitems={getNavItems(this) }
                    />
                <Container>
                    <Bs.Col md={codeSize}>
                      {this.props.children}
                    </Bs.Col>
                    <Bs.Col xsOffset={offset} md={mirrorSize}>
                      <Mirror
                        selected={this.state.selected}></Mirror>
                    </Bs.Col>
                </Container>
            </div>
        );
    }
}


const mapStateToProps = state => {
  return {
    codeToRender: state
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

const AppConnected = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)



export default (AppConnected);
