let compList = [
    "Card",
    "Loader",
    "TextField",
    "ButtonPrimary",
    "Popover",
    "NavMenu",
    "Combobox",
    "Container",
    "Collapse",
    "Modal",
    "Alert",
    "Breadcrumb",
    "Input",
    "Fiddle"
]

export default compList;
