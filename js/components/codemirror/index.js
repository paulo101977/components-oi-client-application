let React = require('react');
import CodeMirror from 'react-codemirror2';

require('../../../node_modules/codemirror/mode/jsx/jsx');

//import code from './code/breadcrumb'
//redux bind method
import { connect } from 'react-redux'

import resolver from './coderesolver';

//import the action to change code
import {changeCode} from "./../../actions"

class Mirror extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      code: resolver('Card'),
      readOnly: true
    }

    this.onChange = this.onChange.bind(this)
  }

  componentWillReceiveProps(nextProps){
    let selected = nextProps.selected;

    let code = require('./../config/template')

    if(selected){
      if(selected === "Fiddle"){
        this.setState({
          readOnly: false,
          code: code
        })
      }
      else {
        this.setState({
          readOnly: true,
          code:resolver(nextProps.selected)
        })
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    //console.log(nextState.code)
    return true;
  }

  onChange(a,b,code){
    this.props.onChangeCode(code)
  }

  render(){
      const options = {
        mode: 'jsx',
        lineNumbers: true,
        readOnly: this.state.readOnly
      }

      const selected = this.props.selected;
      const codeRecovered = localStorage.getItem('code');
      let code = selected ?
          (this.state.readOnly ? resolver(selected) : this.state.code)
          : this.state.code;

      //code = codeRecovered && !this.state.readOnly ? codeRecovered : code;

      return <CodeMirror
                onChange={this.onChange}
                value={code}
                options={options}/>
  }
}

const mapStateToProps = state => {
  return {
    codeToRender: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onChangeCode: codeToRender =>{
      dispatch(changeCode(codeToRender))
    }
  }
}

//append to mirror
const MirrorInstance = connect(
  mapStateToProps,
  mapDispatchToProps
)(Mirror)



export default (MirrorInstance);
