module.exports = (name) =>{
    switch(name){
      case "NavMenu":
        return require("./code/navmenu");
      case "Card":
        return require('./code/card');
      case "Breadcrumb":
        return require('./code/breadcrumb');
      case "ButtonPrimary":
        return require('./code/button');
      case "Collapse":
        return require('./code/collapse');
      case "Combobox":
        return require('./code/combobox');
      case "Alert":
        return require('./code/alert');
      case "Input":
          return require('./code/input');
      case "Loader":
            return require('./code/loader');
      case "Modal":
            return require('./code/modal');
      case "Popover":
            return require('./code/popover');
      case "TextField":
            return require('./code/textfield');
      default:
        return require('./code/breadcrumb')
    }
}
