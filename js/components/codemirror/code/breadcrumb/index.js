
//import * as Bs from "react-bootstrap";
//import React from 'react'
//import ReactDOMServer from 'react-dom/server';

//const file = require('./../../comp_to_render/button');
//import lebab from 'lebab';

/*let name = "Breadcrumb";

console.log('before')
console.log(file)

let str =
`
  var ${name} = require("components_oi")("${name}")
  function ${name}(props) {
    super(props);
  }
  ${name}.prototype.render = ${file}
`*/

//console.log(str)


//var escodegen = require('escodegen');
//var espree = require('espree')

// Optional second options argument with the following default settings
//var {code, warnings} = espree.code(str);
//str = "var Breadcrumb = " + str.toString()
//var esprima = require('esprima')
//var simple = require('jstransform/simple')
//var parsed = esprima.parse(str.toString(),{ jsx: true,tolerant:true })
/*const {code, warnings} = lebab.transform(str,
    ['let',
      'arrow',
      'for-of',
      'for-each',
      'arg-rest',
      'arg-spread' ,
      'obj-method',
      'obj-shorthand',
      'no-strict',
      'exponent',
      'multi-var',
      'class',
      'commonjs',
      'template',
      'default-param',
      'destruct-param',
      'includes'
    ]
  );*/

const code =
`

  
${require("../variables").sample}
import React from "react";

let Breadcrumb = require("components_oi")("Breadcrumb");
${require("../variables").example}


module.exports = ()=>{
    return <Breadcrumb>
                <Breadcrumb.Item href="#">
                    Home
                </Breadcrumb.Item>
                <Breadcrumb.Item href="http://www.minhaoi.com.br">
                    Minha Oi
                </Breadcrumb.Item>
                <Breadcrumb.Item active>
                    Here
                </Breadcrumb.Item>
            </Breadcrumb>;
}
`

module.exports  = `${code}`
