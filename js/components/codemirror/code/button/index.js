module.exports =`


${require("../variables").sample}
import React, { Component } from "react";

let Button = require("components_oi")("Button");
${require("../variables").example}


module.exports = ()=>{
    return <Button label="Just a Button">Just a Button</Button>;
}
`
