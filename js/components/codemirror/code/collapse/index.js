module.exports = `


${require("../variables").sample}
import React, { Component } from "react";

let Collapse = require("components_oi")("Collapse");
${require("../variables").example}


let getItems = ()=>{
    return [0,1,2,3,4,5,6].map((item,key)=>{
        return <Collapse.Item
                    key={key}
                    id={key}
                    pergunta={"Item" + key}>
                        Just a item content.
                        <br></br>
                        <br></br>
                        <br></br>
                    </Collapse.Item>
    })
}

module.exports =
    ()=>{
        return <Collapse>
                    {getItems()}
                </Collapse>
    }

`
