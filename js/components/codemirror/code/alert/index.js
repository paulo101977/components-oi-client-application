module.exports = `


${require("../variables").sample}
import React, { Component } from "react";


let Alert = require("components_oi")("Alert");
${require("../variables").example}



module.exports =
    ()=>{
        return <div>
                    <Alert
                        size=""
                        icon="spinner">
                        Just a alert test!
                    </Alert>
                    <Alert
                        size=""
                        icon="danger">
                        Just a alert test!
                    </Alert>
                    <Alert
                        size=""
                        icon="info">
                        Just a alert test!
                    </Alert>
                    <Alert
                        size=""
                        icon="success">
                        Just a alert test!
                    </Alert>
                    <Alert
                        size=""
                        icon="dollar">
                        Just a alert test!
                    </Alert>
                    <Alert
                        size="mini"
                        icon="danger">
                        Just a alert test!
                    </Alert>
                </div>
    }

`
