module.exports = `


${require("../variables").sample}
import React, { Component } from "react";

let Combobox = require("components_oi")("Combobox");
${require("../variables").example}

let getItems = ()=>{
    return [0,1,2,3,4,5,6].map((item, key)=>{
        return <Combobox.Item key={key}>
                    Just a Item
                </Combobox.Item>
    })
}

module.exports = ()=>{
    return <Combobox placeholder="Just a Combobox">
                {getItems()}
            </Combobox>;
}
`
