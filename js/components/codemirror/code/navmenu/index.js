module.exports =
`

${require("../variables").sample}
import React from "react";

import * as Bs from "react-bootstrap"

import user from '../../img/user.svg';

const compList = require("../../config").default;
${require("../variables").example}

let {
    NavMenu ,
    Popover
  } = require('components_oi')(['NavMenu' , 'Popover'])


  let alertMethod = (event)=>{
      alert('test');
  };

let handleClick = (event, obj)=>{
  event.preventDefault();
  console.log('clicked')
}

let getNavItems = (obj)=>{
      let navitems = [];


      compList.forEach(function(item,index){
          if(item !== "Container"){
              navitems.push(
                  {
                      title: item,
                      href:"#/" + item,
                      key: index,
                      handleClick: (event) => handleClick(event, obj),
                      active: index == 0 ? true : false,
                      item: item
                  }
              )
          }
      })

      return navitems;
  }

  //test submenu
  let submenu = [
      {
          title: "Home",
          links: [
              {
                  handleNavLink: alertMethod,
                  linkTitle: 'just a link',
                  href:"#/home",
                  linkContent: "test"
              }
          ]
      },
      {
          title: "About",
          links: [
              {
                  handleNavLink: alertMethod,
                  linkTitle: 'just a link',
                  href:"#/home",
                  linkContent: "test"
              }
          ]
      },
      {
          title: "More",
          links: [
              {
                  handleNavLink: alertMethod,
                  linkTitle: 'just a link',
                  href:"#/home",
                  linkContent: "test"
              }
          ]
      }
  ]

//test menu right content
let rightcontent =
    <Popover
        left='0'
        name='sair'
        className='xs-hidden'
        content={<Bs.Button className='link px-2 py-1 sair'>SAIR</Bs.Button>}>
        <span>
            <img className='mr-1' src={user} />
            <span className='sair'>Sair</span>
        </span>
    </Popover>;

module.exports = ()=>{
    return <NavMenu
              rightcontent={rightcontent}
              submenu={submenu}
              navitems={getNavItems(this) }
              />
    }

`
